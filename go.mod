module editors

go 1.15

require (
	gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang v0.0.0-20220322011350-df509f780b5c
	github.com/carousell/fasthttp-prometheus-middleware v1.0.6
	github.com/fasthttp/router v1.4.11
	github.com/gocql/gocql v1.2.0
	github.com/roger-russel/fasthttp-router-middleware v1.0.0
	github.com/stretchr/testify v1.7.0
	github.com/valyala/fasthttp v1.43.0
	gopkg.in/yaml.v2 v2.4.0
	schneider.vip/problem v1.6.0
)
