package logic

import (
	"context"
	"editors/entities"
	"encoding/json"
	"net/http"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"github.com/valyala/fasthttp"
	"schneider.vip/problem"
)

type EditorsLogic struct {
}

func (s *EditorsLogic) ProcessEditorsLogic(context context.Context, ctx *fasthttp.RequestCtx, project, activityLevel, year, month string, session *gocql.Session, rLogger *logger.Logger) (*problem.Problem, entities.EditorsByCountryResponse, int) {
	var err error
	var problemData *problem.Problem
	var editorsData = []entities.Country{}
	var response = entities.EditorsByCountryResponse{Items: make([]entities.EditorsByCountry, 0)}

	var countriesJSON string

	query := `SELECT "countriesJSON" FROM "local_group_default_T_editors_bycountry".data WHERE "_domain" = 'analytics.wikimedia.org' and project = ? and "activity-level" = ? AND year = ? AND month = ?`
	if err := session.Query(query, project, activityLevel, year, month).WithContext(ctx).Consistency(gocql.One).Scan(&countriesJSON); err != nil {
		rLogger.Log(logger.ERROR, "Query failed; %s", err)
		problemData = problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusBadRequest)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusBadRequest),
			problem.Detail(err.Error()),
			problem.Custom("uri", ctx.Request.RequestURI))
		return problemData, entities.EditorsByCountryResponse{}, http.StatusBadRequest
	}

	if err = json.Unmarshal([]byte(countriesJSON), &editorsData); err != nil {
		rLogger.Log(logger.ERROR, "Unable to unmarshal returned data: %s", err)
		problemData = problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", ctx.Request.RequestURI))
		return problemData, entities.EditorsByCountryResponse{}, http.StatusInternalServerError
	}

	var countries = []entities.Country{}

	for _, s := range editorsData {
		countries = append(countries, entities.Country{
			Country:     s.Country,
			EditorsCeil: s.EditorsCeil,
		})
	}

	response.Items = append(response.Items, entities.EditorsByCountry{
		Project:       project,
		ActivityLevel: activityLevel,
		Year:          year,
		Month:         month,
		Countries:     countries,
	})
	return nil, response, 0
}
