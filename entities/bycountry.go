package entities

type EditorsByCountryResponse struct {
	Items []EditorsByCountry `json:"items"`
}

type EditorsByCountry struct {
	Project       string    `json:"project"`
	ActivityLevel string    `json:"activity-level"`
	Year          string    `json:"year"`
	Month         string    `json:"month"`
	Countries     []Country `json:"Countries"`
}

type Country struct {
	Country     string `json:"country"`
	EditorsCeil int    `json:"editors-ceil"`
}
