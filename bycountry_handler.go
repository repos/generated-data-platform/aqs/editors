package main

import (
	"context"
	"editors/logic"
	"encoding/json"
	"net/http"
	"time"

	"github.com/valyala/fasthttp"

	"gerrit.wikimedia.org/r/mediawiki/services/servicelib-golang/logger"
	"github.com/gocql/gocql"
	"schneider.vip/problem"
)

type EditorsByCountryHandler struct {
	logger  *logger.Logger
	session *gocql.Session
	logic   *logic.EditorsLogic
	config  *Config
}

func (s *EditorsByCountryHandler) HandleFastHTTP(ctx *fasthttp.RequestCtx) {
	var err error

	project := ctx.UserValue("project").(string)
	activityLevel := ctx.UserValue("activity-level").(string)
	year := ctx.UserValue("year").(string)
	month := ctx.UserValue("month").(string)

	c, cancel := context.WithTimeout(ctx, time.Duration(s.config.ContextTimeout)*time.Millisecond)
	pbm, response, pbmStatus := s.logic.ProcessEditorsLogic(c, ctx, project, activityLevel, year, month, s.session, s.logger)
	defer cancel()

	if pbm != nil {
		problemResp, _ := json.Marshal(pbm)
		ctx.SetBody(problemResp)
		ctx.SetStatusCode(pbmStatus)
		return
	}

	var data []byte
	if data, err = json.MarshalIndent(response, "", " "); err != nil {
		s.logger.Log(logger.ERROR, "Unable to marshal response object: %s", err)
		problem.New(
			problem.Type("about:blank"),
			problem.Title(http.StatusText(http.StatusInternalServerError)),
			problem.Custom("method", http.MethodGet),
			problem.Status(http.StatusInternalServerError),
			problem.Detail(err.Error()),
			problem.Custom("uri", ctx.Request.RequestURI()))
		return
	}
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody([]byte(data))
}
